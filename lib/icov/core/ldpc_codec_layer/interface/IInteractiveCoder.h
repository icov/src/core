/****************************************************************************/
/** @addtogroup ICOV
 * ICOV, open source Interactive Coder for Omnidirectional Videos.
 */

/** @addtogroup ldpc_codec_layer
 *  @ingroup icov
 */

/** @file IInteractiveCoder.h
 *  @brief
 *  @ingroup interactive-coder
 *  @author Sebastien Bellenous (SIROCCO) <sebastien.bellenous@inria.fr>
 *  @version 0.2.1
 *  @date 2023
 *  @copyright Copyright (C) 2023 by Inria. All rights reserved.
 * The copyright in this software is being made available under
 * the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * <br/>
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * <br/>
 * <b> For using ICOV under terms that will not comply with the GNU
 * AGPL, please contact Inria about acquiring an ICOV Professional
 * Edition License. </b>
 * <br/>
 * See contact section in README file for more information.
 * <br/>
 * This software was developed at:
 * Inria Rennes - Bretagne Atlantique
 * Campus Universitaire de Beaulieu
 * 35042 Rennes Cedex
 * France
 * <br/>
 * If you have questions regarding the use of this software, please contact
 * Inria using the contact section in the README file.
 * <br/>
 * This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
 * WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
 *
 *****************************************************************************/

#pragma once

#include "icov_assert.h"
#include "icov_exception.h"
#include "icov_types.h"
#include <algorithm>
#include <cstddef>
#include <sys/types.h>

namespace icov {
namespace ldpc_codec_layer {

class IInteractiveCoder {
public:
  enum SyndromeType {
    SyndromeType_SOURCE = 0,
    SyndromeType_NO_CODING = 1,
    SyndromeType_NORMAL = 2,
    SyndromeType_COUNT
  };

  static struct t_params { bool CheckBitplaneEncoding; } params;
  virtual ~IInteractiveCoder() = default;
  virtual size_t get_n() const = 0;
  virtual int get_m(const NumcodeIdx &numcodeIdx) const = 0;
  virtual Rate get_r(const NumcodeIdx &numcodeIdx) const = 0;
  virtual Rate get_r(int m, int n) const = 0;

  static SyndromeType getSyndromeType(uint syndromeNumcode) {
    return static_cast<SyndromeType>(
        std::min(syndromeNumcode, (uint)SyndromeType_NORMAL));
  }

  static NumcodeIdx getNumcodeIdx(uint syndromeNumcode) {
    icov::assert::equal(getSyndromeType(syndromeNumcode), SyndromeType_NORMAL,
                        ICOV_DEBUG_HEADER("Bad syndrome type"));
    return static_cast<NumcodeIdx>(syndromeNumcode -
                                   getSyndromeNumcodeOffset());
  }

  static unsigned char getSyndromeNumcodeOffset() {
    return (uint)SyndromeType_NORMAL;
  }

  // Function for calculating inverse quantized size based on SyndromeType
  int inverseQuantizedSize(uint input) const {

    SyndromeType s_type = getSyndromeType(input);

    switch (s_type) {
    case SyndromeType_SOURCE:
      return (int)this->get_n();
    case SyndromeType_NO_CODING:
      return 0;
    case SyndromeType_NORMAL:
      return (int)this->get_m(input - ldpc_codec_layer::IInteractiveCoder::
                                          SyndromeType::SyndromeType_NORMAL);
    default:
      ICOV_THROW_INVALID_ARGUMENT(__func__);
      break;
    }
    return -1;
  }
};
} // namespace ldpc_codec_layer
} // namespace icov
