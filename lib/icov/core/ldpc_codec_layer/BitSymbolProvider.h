#pragma once

#include "FastArray.h"
#include "ldpc_codec_layer/QaryDistribution.h"
#include <sys/types.h>
#include <vector>

#define Z_STEP 16
#define Z_LIMIT 1024

namespace icov {
namespace ldpc_codec_layer {

/**
 * @brief The BitSymbolProvider class
 *
 * A class that provides methods for computing bits from symbols, and vice
 * versa.
 *
 * BitSymbolProvider is a class that provides a way to convert between
 * bitplanes and symbols. In the context of channel decoding, a set of bitplanes
 * is a 2D array of binary values representing the coded bits at a specific
 * quantization level for a set of pixels (symbols). Each bitplane is an array
 * of bits at a specific position in the binary representation associated to the
 * pixels in a specific quantized and transformed block unit. BitSymbolProvider
 * makes possible the encoding and decoding of these symbols by providing
 * methods to convert the bits to and from symbols.
 *
 * A BitSymbolProvider is a class that provides access to bit codewords used in
 * LDPC (Low Density Parity Check) coding. It is specifically designed to supply
 * bit for LDPC encoding and decoding operations. The BitSymbolProvider
 * class encapsulates the functionality related to managing and retrieving
 * bitplanes from symbols, which are binary values representing information to
 * be encoded or decoded using LDPC codes. It contains methods to access the
 * source bits or symbols, perform operations on them, and provide them to the
 * LDPC coder for encoding or decoding purposes.
 * A bitplane is a way to represent a binary image by dividing it into multiple
 * planes, where each plane represents the value of a single bit. The first
 * bitplane represents sign value ( 1 means value < 0) of the
 * quantized-transformed pixel value. The second bitplane is the most
 * significant bit of each pixel, while the last bitplane represents the least
 * significant bit. Bitplanes are commonly used in image compression algorithms,
 * such as JPEG2000, where each bitplane is compressed separately and then
 * combined to form the final compressed image. By compressing each bitplane
 * separately, the compression algorithm can take advantage of local
 * correlations between neighboring pixels at different bit depths, which can
 * improve the overall compression performance.
 *
 */
class BitSymbolProvider {
public:
  /**
   * @brief compute_bits
   * Compute bits from symboles
   * @param input
   * @return
   */
  static Bitplanes compute_bits(const SymboleVec &input);

  /**
   * @brief compute_symboles
   * Compute symboles from bits
   * @param input
   * @return
   */
  static SymboleVec compute_symboles(const Bitplanes &input);

  void inject_bitplane(SymboleVec &output, const BitVec &input, int bp_shift);

  ///
  /// \brief BitSymbolProvider
  /// \param n
  ///
  BitSymbolProvider(int n);

  ///
  /// \brief BitSymbolProvider ctor used in encode mode
  /// \param src_symbols
  ///
  BitSymbolProvider(const SymboleVec &src_symbols);

  ///
  /// \brief set_si for decoding
  /// \param si
  void set_si(const SIVec &si);
  void set_si_and_compute_pz_params(
      const SIVec &si,
      const QaryDistribution::quantization_params &quantParams);
  void set_si_and_qaryDistribution(const SIVec &si,
                                   const QaryDistribution &dist);

  void reset_src();

  //  int z_min() const;
  //  int z_max() const;

  //  float y_min() const;
  //  float y_max() const;

  //  int q_idx() const;

  //  // q
  //  float q0() const;

  //  // q'
  //  float q1() const;

  //  // z associated with q0 = min(|zk|)
  //  int z0() const;

  //..0..0 [q'..q' q q'..q'] 0..0..
  // float pz(int z) const;

  //  float pz(int zmin, int zmax) const;

  // L
  int bitplane_count() const;
  int code_length() const;

  const Bitplanes &src_bits() const { return m_src_bits; }
  const SymboleVec &src_symboles() const { return m_src_symboles; }
  const SIVec &si_symboles() const { return m_si_symboles; }
  SymboleVec getDiff() const;

  void injectBitplane(uint i, BitVec x);
  void resetBitplanes(int count);

  const QaryDistribution &distribution() const;
  QaryDistribution &distribution();

private:
  QaryDistribution m_distribution;
  SymboleVec m_src_symboles;
  SIVec m_si_symboles;
  Bitplanes m_src_bits;
};
} // namespace ldpc_codec_layer
} // namespace icov
