/****************************************************************************/
/** @addtogroup ICOV
 *  ICOV, open source Interactive Coder for Omnidirectional Videos.
 */

/** @addtogroup common
 *  @ingroup ICOV
 */

/** @file IMedia.h
 *  @brief Defines interface for IMedia object.
 *  @ingroup common
 *  @author Sebastien Bellenous (SIROCCO) <sebastien.bellenous@inria.fr>
 *  @version 1.5.0
 *  @date 2023
 *  @copyright Copyright (C) 2023 by Inria. All rights reserved.
 * The copyright in this software is being made available under
 * the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * <br/>
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * <br/>
 * <b> For using ICOV under terms that will not comply with the GNU
 * AGPL, please contact Inria about acquiring an ICOV Professional
 * Edition License. </b>
 * <br/>
 * See contact section in README file for more information.
 * <br/>
 * This software was developed at:
 * Inria Rennes - Bretagne Atlantique
 * Campus Universitaire de Beaulieu
 * 35042 Rennes Cedex
 * France
 * <br/>
 * If you have questions regarding the use of this software, please contact
 * Inria using the contact section in the README file.
 * <br/>
 * This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
 * WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
 *
 *****************************************************************************/

#pragma once

#include "icov_assert.h"
#include "icov_path.h"
#include "icov_request.h"
#include <boost/any.hpp>
#include <boost/filesystem.hpp>
#include <boost/filesystem/operations.hpp>
#include <fstream>
#include <ios>
#include <memory>
#include <ostream>

namespace fs = boost::filesystem;

namespace icov {

enum eMediaType {
  UNKNOWN = -1,
  IMAGE = 0,
  VIDEO = 1,
  MODEL_3D = 2,
  ICOV = 3,
  COUNT,
  ERROR
};

inline std::string mediaTypeStr(eMediaType t)
{
  switch (t)
  {
  case IMAGE:
      return "image";
  case VIDEO:
      return "video";
  case MODEL_3D:
      return "3D model";
  case ICOV:
      return "ICOV";
  case COUNT:
      return "count";
  case ERROR:
      return "error";
  case UNKNOWN:
  default:
      return "unknown";
  }
}

class IMedia {
public:
  virtual ~IMedia() = default;

  explicit IMedia(const fs::path &path,
                  int media_type = (int)eMediaType::UNKNOWN)
      : m_type(media_type), m_path(fs::absolute(path)), m_width(0), m_height(0),
        m_frameCount(0)

  {
    ICOV_ASSERT_MSG(m_type != eMediaType::COUNT && m_type != eMediaType::ERROR,
                    "Media type is unsupported");
    ICOV_WARNING_MSG(m_type != eMediaType::UNKNOWN, "Media type is unknown");
  }

  inline size_t getWidth() const { return m_width; }
  inline size_t getHeight() const { return m_height; }
  inline int getType() const { return m_type; }
  inline frame_t getFrameCount() const { return m_frameCount; }
  inline size_t getPixelCount() const { return m_width * m_height; }
  inline const boost::filesystem::path &getPath() const { return m_path; }

protected:
  int m_type;
  fs::path m_path;
  size_t m_width;
  size_t m_height;
  frame_t m_frameCount;

private:
  IMedia() = default;
  //  IMedia(const IMedia &) = default;
};

/****************************************************************************/
/** @ingroup common
 *  @brief Defines the interface for any IInputMedia.
 */
template <typename T> class IInputMedia : public IMedia {
private:
  IInputMedia() = default;

protected:
  T m_frameData;
  frame_t m_frameID;

public:
  // using IMedia::IMedia;

  IInputMedia(const fs::path &path, int media_type = (int)eMediaType::UNKNOWN)
      : IMedia(path, media_type), m_frameID(0) {
    // icov::path::assert_file_exists(path.string());
  }

  virtual void setFrame(frame_t frame) { m_frameID = frame; }
  inline frame_t getFrameID() const { return m_frameID; }

  virtual inline const T &getCurrentFrame() const { return m_frameData; }
  inline virtual const boost::any getProperty(std::string id) {
    return boost::any(id);
  }

  virtual void writeInfos(std::ofstream &output, int key,
                          void *param = nullptr) = 0; //@todo : const ?
  virtual ~IInputMedia() = default;
};

/****************************************************************************/
/** @ingroup common
 *  @brief Defines the interface for any IOutputMedia.
 */
class IOutputMedia : public IMedia {
protected:
  std::unique_ptr<std::ostream> m_outputStream;

public:
  explicit IOutputMedia(const fs::path &path, int media_type = (int)eMediaType::UNKNOWN)
      : IMedia(path, media_type), m_outputStream(std::make_unique<std::ofstream>(path))
  {
    icov::path::assert_file_exists(path.string());
    icov::assert::not_null(m_outputStream.get());
  }

  virtual ~IOutputMedia() = default;
  inline std::ostream &stream() { return *m_outputStream; }
};

template <typename T> class IImage : public IInputMedia<T> {
public:
  explicit IImage(const fs::path &path)
      : IInputMedia<T>(path, (int)eMediaType::IMAGE) {
    /*image = 1 frame only*/
    this->m_frameCount = 1;
    icov::path::assert_file_exists(path.string());
  }

  // IMedia interface
public:
  void setFrame(frame_t frame = 0) override {
    this->m_frameID = 0; /*image = 1 frame only*/
  }
};

template <typename T> class IVideo : public IInputMedia<T> {
public:
  explicit IVideo(const fs::path &path)
      : IInputMedia<T>(path, (int)eMediaType::VIDEO) {
    icov::path::assert_file_exists(path.string());
  }
};

template <typename T> class IModel3D : public IInputMedia<T> {
public:
  explicit IModel3D(const fs::path &path)
      : IInputMedia<T>(path, (int)eMediaType::MODEL_3D) {
    icov::path::assert_file_exists(path.string());
  }
  virtual ~IModel3D() = default;
};

template <typename T> class IInputIcov : public IInputMedia<T> {
public:
  explicit IInputIcov(const fs::path &path)
      : IInputMedia<T>(path, (int)eMediaType::ICOV) {}
  virtual ~IInputIcov() = default;
};

class IOutputIcov : public IOutputMedia {
protected:
public:
  explicit IOutputIcov(const fs::path &path)
      : IOutputMedia(path, (int)eMediaType::ICOV) {}
  virtual ~IOutputIcov() = default;
};

} // namespace icov
