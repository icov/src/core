#pragma once

#if _MSC_VER
#ifndef _HAS_STD_BYTE
#define _HAS_STD_BYTE 0
#define _USE_MATH_DEFINES
#endif
#ifndef NOMINMAX
#define NOMINMAX
#endif
#include <Windows.h>
#endif
