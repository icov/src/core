#pragma once

#include <limits>
#include <sstream>
#include <string>
namespace icov {

typedef float angle_t;
typedef unsigned long frame_t;

struct viewpoint_t
{
    angle_t angleX;
    angle_t angleY;

    viewpoint_t() : angleX(0), angleY(0){};
    viewpoint_t(angle_t p_angleX, angle_t p_angleY)
        : angleX(p_angleX), angleY(p_angleY){};
};

struct request_t
{
    viewpoint_t viewpoint;
    frame_t frameID;

    request_t() : viewpoint(), frameID(0) {}
    request_t(viewpoint_t p_viewpoint, frame_t p_frameID)
        : viewpoint(p_viewpoint), frameID(p_frameID) {}

    request_t(angle_t p_angleX, angle_t p_angleY, frame_t p_frameID)
        : viewpoint(p_angleX, p_angleY), frameID(p_frameID) {}

    static request_t undefined() {
      return request_t(viewpoint_t(std::numeric_limits<angle_t>::max(),
                                   std::numeric_limits<angle_t>::max()),
                       std::numeric_limits<frame_t>::max());
    }
};

inline bool operator==(const viewpoint_t &a, const viewpoint_t &b)
{
    return a.angleX == b.angleX && a.angleY == b.angleY;
}

inline bool operator==(const request_t &a, const request_t &b)
{
    return a.viewpoint == b.viewpoint && a.frameID == b.frameID;
}

inline bool operator!=(const viewpoint_t &a, const viewpoint_t &b)
{
    return !(a == b);
}

inline bool operator!=(const request_t &a, const request_t &b)
{
    return !(a == b);
}

inline std::string to_string(const viewpoint_t v) {
    std::stringstream sst;
    sst << "{x=" << v.angleX << ", y=" << v.angleY << "}";
    return sst.str();
}

inline std::string to_string(const request_t r) {
    std::stringstream sst;

    sst << "{viewpoint= " << to_string(r.viewpoint) << ", frame=" << r.frameID
        << "}";

    return sst.str();
}

} // namespace icov
