#include "RequestManager.h"
#include "icov_assert.h"
#include "icov_exception.h"
#include "icov_log.h"
#include "icov_request.h"
#include "omnidirectional_layer/Block.h"
#include "omnidirectional_layer/icov_geometry.h"
#include "omnidirectional_layer/interface/IAccessBlockManager.h"
#include "omnidirectional_layer/interface/IBlockOrdering.h"
#include "omnidirectional_layer/interface/IBlockPredictionManager.h"
#include "omnidirectional_layer/interface/IBlockProcessor.h"
#include "omnidirectional_layer/interface/IPicture.h"
#include "omnidirectional_layer/interface/IViewportManager.h"
#include <limits>

namespace icov {
namespace omnidirectional_layer {

const std::shared_ptr<IBlockPredictionManager> &
RequestManager::blockPrediction() const {
  return m_blockPrediction;
}

void RequestManager::setBlockPrediction(
    const std::shared_ptr<IBlockPredictionManager> &newBlockPrediction) {
  m_blockPrediction = newBlockPrediction;
}

const request_t &RequestManager::request() const
{
  return m_request;
}

void RequestManager::start()
{
  icov::assert::not_null(m_blockProcessor.get(),
                         ICOV_DEBUG_HEADER("Block processor is not allocated"));
  icov::assert::not_null(
      m_accessBlocks.get(),
      ICOV_DEBUG_HEADER("Access block manager is not allocated"));
  icov::assert::not_null(m_picture.get(),
                         ICOV_DEBUG_HEADER("Picture is not allocated"));
  m_accessBlocks->buildAccessBlocks(*m_picture, *m_viewport);
  m_request.viewpoint.angleX = std::numeric_limits<float>::max();
  m_request.viewpoint.angleY = std::numeric_limits<float>::max();
  m_request.frameID = std::numeric_limits<int>::max();
  m_previousDecoded.resize(m_picture->blockCount(), 0);
}

void icov::omnidirectional_layer::RequestManager::processBlock(bool is_access_block,
                                                               const icov::omnidirectional_layer::Block &current_block)
{
  IBlockPredictionManager::prediction_t prediction = 0;

  try {

    if (!is_access_block)
      prediction = m_blockPrediction->getPrediction(current_block, *m_ordering);

    if (m_blockProcessor) {
      m_blockProcessor->processBlock(
          request(), current_block, prediction,
          m_previousDecoded[m_picture->getBlockIndexPos(
              current_block.getIdSelf())] -
              1);
    } else {
      ICOV_TRACE << "Empty processing block " << current_block.getIdSelf()
                 << " with prediction id " << prediction;
    }

    m_previousDecoded[m_picture->getBlockIndexPos(current_block.getIdSelf())] =
        request().frameID + 1;

  } catch (...) {
    ICOV_ERROR << "Error while processing block " << current_block.getIdSelf()
               << " with prediction id " << prediction;

    m_previousDecoded[m_picture->getBlockIndexPos(current_block.getIdSelf())] =
        0;

    throw;
  }
}

void icov::omnidirectional_layer::RequestManager::processViewport() {

  auto current_block = m_accessBlocks->findAccessBlock(*m_viewport);

  bool is_access_block = current_block->isAccessBlock();

  icov::assert::check(is_access_block,
                      ICOV_DEBUG_HEADER("Access block flag not set"));
  icov::assert::check(
      m_picture->isValidBlock(current_block),
      ICOV_DEBUG_HEADER("No access block was found in the viewport"));

  while (m_picture->isValidBlock(current_block)) {

      processBlock(is_access_block, *current_block);

      // if same position as previous, take next in previous ordering
      // else find next block
      current_block = m_ordering->getNextBlock(*current_block, *m_viewport);

      is_access_block = false;
  }
}

void RequestManager::resetCache() {
  for (auto &e : m_previousDecoded)
      e = 0;
}

void RequestManager::update(const request_t &r) {
  if (r == m_request)
      return;

  ICOV_DEBUG << "Set request " << to_string(r);

  m_request = r;

  icov::assert::not_null(m_ordering.get(),
                         ICOV_DEBUG_HEADER("Ordering manager cannot be null"));
  icov::assert::not_null(m_viewport.get(),
                         ICOV_DEBUG_HEADER("Viewport manager cannot be null"));
  icov::assert::not_null(
      m_accessBlocks.get(),
      ICOV_DEBUG_HEADER("Access blocks manager manager cannot be null"));

  icov::assert::not_null(
      m_blockPrediction.get(),
      ICOV_DEBUG_HEADER("Predictions manager manager cannot be null"));


  m_ordering->reset();
  m_viewport->viewportGeometry().setPov(
      angle_radian(r.viewpoint.angleX, r.viewpoint.angleY));

  if (m_blockProcessor)
      m_blockProcessor->beforeOrderingLoop(request());

  processViewport();

  if (m_blockProcessor)
      m_blockProcessor->afterOrderingLoop(request());

}

const std::shared_ptr<IAccessBlockManager> &
RequestManager::accessBlocks() const {
  return m_accessBlocks;
}

void RequestManager::setAccessBlocks(
    const std::shared_ptr<IAccessBlockManager> &newAccessBlocks) {
  m_accessBlocks = newAccessBlocks;
}

const std::shared_ptr<IBlockOrdering> &RequestManager::ordering() const {
  return m_ordering;
}

void RequestManager::setOrdering(
    const std::shared_ptr<IBlockOrdering> &newOrdering) {
  m_ordering = newOrdering;
}

const std::shared_ptr<IViewportManager> &RequestManager::viewport() const {
  return m_viewport;
}

void RequestManager::setViewport(
    const std::shared_ptr<IViewportManager> &newViewport) {
  m_viewport = newViewport;
}

const std::shared_ptr<IPicture> &RequestManager::picture() const {
  return m_picture;
}

void RequestManager::setPicture(const std::shared_ptr<IPicture> &newPicture) {
  m_picture = newPicture;
}

const std::shared_ptr<IBlockProcessor> &RequestManager::blockProcessor() const {
  return m_blockProcessor;
}

void RequestManager::setBlockProcessor(
    const std::shared_ptr<IBlockProcessor> &newBlockProcessor) {
  m_blockProcessor = newBlockProcessor;
}
} // namespace omnidirectional_layer
} // namespace icov
