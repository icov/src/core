#include "BinaryOutputIcov.h"

#include "icov_exception.h"
#include "omnidirectional_layer/interface/IBlockPredictionManager.h"
#include "video_layer/encoded_stream.h"
#include "video_layer/icov_stream.h"
#include <boost/serialization/array_wrapper.hpp>
#include <ios>
#include <limits>
#include <memory>
#include <string>
namespace icov {
namespace encoder {

BinaryOutputIcov::BinaryOutputIcov(const boost::filesystem::path &path)
    : OutputIcovBase(path, std::ios_base::binary) {}

void BinaryOutputIcov::writeQaryParams(
    const ldpc_codec_layer::QaryDistribution &bsp) {
    if (!bsp.codingflag())
    {
        // std::cout << "SI IDENTICAL" << std::endl;
        //    std::cout << "Q 0" << std::endl;
        icov::io::write_value<byte>(
            getStream(bitstream::eIcovStream::ENCODE_PARAMS), 0, false);
        m_total_write_byte[eWriteCounter::ENC_BLOCK_RATE_SIZE] += sizeof(byte);
    }
    else
    {
        icov::bitstream::writeQaryParams(m_qaryParams, bsp.getRawQ0(), bsp.getRawZmin(), bsp.getRawZmax());
        icov::io::write_array(getStream(bitstream::eIcovStream::ENCODE_PARAMS),
                              m_qaryParams, QARY_PARAMS_SIZE, false);
        m_total_write_byte[eWriteCounter::ENC_BLOCK_RATE_SIZE] += QARY_PARAMS_SIZE * sizeof(byte);
    }
}

void BinaryOutputIcov::writeContentParameterSet() {
  boost::archive::binary_oarchive o_archive(
      *m_fileParts[bitstream::eIcovStream::MEDIA_HEADER],
      boost::archive::no_header);
  o_archive &m_contentParameterSet;
  flush(bitstream::eIcovStream::MEDIA_HEADER);
}

void BinaryOutputIcov::writeSyndrome(const Syndrome &s) {
  const int byte_size = bitstream::bitToByteCount(s.size());
  byte darr[byte_size];

  std::fill(darr, darr + byte_size, 0);

  bitstream::write_bits(s, darr, 0);

  //  for (int i = 0; i < byte_size; ++i) {
  //        stream() << (ushort)darr[i] << " ";
  //  }
  //  stream() << std::endl;

  int lost = bitstream::byteToBitCount(byte_size) - s.size();

  m_total_write_byte[eWriteCounter::ENC_BLOCK_SYNDROME_SIZE] += byte_size;

  icov::io::write_array(*m_fileParts[bitstream::eIcovStream::SYNDROME], darr,
                        byte_size, false);
}

void BinaryOutputIcov::writeFrameSize() {
  icov::io::write_value<uint32_t>(
      *m_fileParts[bitstream::eIcovStream::FRAMES_SIZE],
      m_total_write_byte[eWriteCounter::ENC_FRAME_RATE_SIZE], false);
  icov::io::write_value<uint32_t>(
      *m_fileParts[bitstream::eIcovStream::FRAMES_SIZE],
      m_total_write_byte[eWriteCounter::ENC_FRAME_SYNDROME_SIZE], false);

  m_total_write_byte[eWriteCounter::ENC_FRAME_RATE_SIZE] = 0;
  m_total_write_byte[eWriteCounter::ENC_FRAME_SYNDROME_SIZE] = 0;

  // flush bitstream at the end of the frame encoding
  this->flush(bitstream::eIcovStream::COUNT);
}

void BinaryOutputIcov::writeBlockSize() {

  icov::io::write_value<uint32_t>(
      *m_fileParts[bitstream::eIcovStream::BLOCKS_SIZE],
      m_total_write_byte[eWriteCounter::ENC_BLOCK_RATE_SIZE], false);
  icov::io::write_value<uint32_t>(
      *m_fileParts[bitstream::eIcovStream::BLOCKS_SIZE],
      m_total_write_byte[eWriteCounter::ENC_BLOCK_SYNDROME_SIZE], false);

  m_total_write_byte[eWriteCounter::ENC_FRAME_RATE_SIZE] +=
      m_total_write_byte[eWriteCounter::ENC_BLOCK_RATE_SIZE];
  m_total_write_byte[eWriteCounter::ENC_FRAME_SYNDROME_SIZE] +=
      m_total_write_byte[eWriteCounter::ENC_BLOCK_SYNDROME_SIZE];
  
  m_total_write_byte[eWriteCounter::ENC_BLOCK_RATE_SIZE] = 0;
  m_total_write_byte[eWriteCounter::ENC_BLOCK_SYNDROME_SIZE] = 0;
}

void BinaryOutputIcov::writeBitplaneCount(const uint &v) {
  writeSIRate(v, 0, 0);
}

// void BinaryOutputIcov::writeSIRates(const NumcodeIdxVec &v) {
//  rates_count += v.size() * sizeof(rate_t);
//  // CHECKED RATES
//  std::cout << "WRITE RATES";
//  for (auto r : v) {
//    writeSIRate(r);
//  }
//  std::cout << std::endl;
//}

void BinaryOutputIcov::writeSIRate(const NumcodeIdx &v, const Rate &r,
                                   const uint &n) {

  if (v > std::numeric_limits<rate_t>::max()) {
        ICOV_THROW_OUT_OF_RANGE(
            "Value " + std::to_string(v) +
            " overflows the max possible value " +
            std::to_string(std::numeric_limits<rate_t>::max()));
  }

  icov::io::write_value<rate_t>(
      *m_fileParts[bitstream::eIcovStream::ENCODE_PARAMS],
      static_cast<rate_t>(v), true);

  m_total_write_byte[eWriteCounter::ENC_BLOCK_RATE_SIZE] += sizeof(rate_t);
}

void BinaryOutputIcov::writeSIMode(const uint &v) {
  icov::io::write_value<byte>(
      *m_fileParts[bitstream::eIcovStream::ENCODE_PARAMS],
      static_cast<rate_t>(v), true);
  m_total_write_byte[eWriteCounter::ENC_BLOCK_RATE_SIZE] += sizeof(byte);
}

} // namespace encoder
} // namespace icov

void icov::encoder::BinaryOutputIcov::startWriteBlock(
    const icov::omnidirectional_layer::Block &b) {}

void icov::encoder::BinaryOutputIcov::startWritePrediction(
    icov::omnidirectional_layer::IBlockPredictionManager::prediction_t pred) {}

void icov::encoder::BinaryOutputIcov::endWritePrediction(
    omnidirectional_layer::IBlockPredictionManager::prediction_t pred) {}

void icov::encoder::BinaryOutputIcov::endWriteBlock(
    const omnidirectional_layer::Block &b) {}

void icov::encoder::BinaryOutputIcov::writeStoredSyndromeSize(
    const NumcodeIdx &v, const Rate &r, const uint &m) {
  writeSIRate(v, r, m);
}

void icov::encoder::BinaryOutputIcov::writeMse(const float &mse)
{
  icov::io::write_value<float>(*m_fileParts[bitstream::eIcovStream::METRICS],
                               mse, false);
}

void icov::encoder::BinaryOutputIcov::endIcov() {
  // ADD 0xFFFFFF at the end of the stream
  for (const auto &p : m_fileParts) {
        if (p.first != bitstream::eIcovStream::MEDIA_HEADER)
          for (int i = 0; i < 3; ++i)
            icov::io::write_value<byte>(*m_fileParts[p.first], 255, false);
        this->flush(p.first);
  }
}
