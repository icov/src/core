/****************************************************************************/
/** @addtogroup ICOV
 *  ICOV, open source Interactive Coder for Omnidirectional Videos.
 */

/** @addtogroup Encoder
 *  ICOV Encoder Lib
 *  @ingroup ICOV
 */

/** @file SourceMedia.h
 *  @brief Defines an actual ICOV SourceMedia.
 *  @ingroup SourceMedia
 *  @author Sebastien Bellenous (SIROCCO) <sebastien.bellenous@inria.fr>
 *  @version 1.5.0
 *  @date 2023
 *  @copyright Copyright (C) 2023 by Inria. All rights reserved.
 * The copyright in this software is being made available under
 * the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * <br/>
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * <br/>
 * <b> For using ICOV under terms that will not comply with the GNU
 * AGPL, please contact Inria about acquiring an ICOV Professional
 * Edition License. </b>
 * <br/>
 * See contact section in README file for more information.
 * <br/>
 * This software was developed at:
 * Inria Rennes - Bretagne Atlantique
 * Campus Universitaire de Beaulieu
 * 35042 Rennes Cedex
 * France
 * <br/>
 * If you have questions regarding the use of this software, please contact
 * Inria using the contact section in the README file.
 * <br/>
 * This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
 * WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
 *
 *****************************************************************************/

#pragma once

#include "Media.h"
#include <boost/algorithm/string.hpp>
#include <opencv2/opencv.hpp>
#include <vector>

namespace icov {
namespace encoder {

using InputMedia = icov::IInputMedia<cv::Mat>;

class InputImage : public icov::IImage<cv::Mat> {
public:
  InputImage(const fs::path &path);
  ~InputImage() = default;

  // IMedia interface
  void writeInfos(std::ofstream &output, int key,
                  void *param = nullptr) override;
};

class InputGop : public icov::IInputMedia<cv::Mat> {
public:
  InputGop(const fs::path &path);
  ~InputGop() = default;

  // IMedia interface
  void setFrame(frame_t i) override;
  void writeInfos(std::ofstream &output, int key,
                  void *param = nullptr) override;

private:
  std::vector<std::string> m_frames;
};

class InputVideo : public icov::IVideo<cv::Mat> {
protected:
  cv::VideoCapture m_videoCV;

public:
  InputVideo(const fs::path &path);
  ~InputVideo() = default;

  // IMedia interface
  void setFrame(frame_t i) override;
  void writeInfos(std::ofstream &output, int key,
                  void *param = nullptr) override;
};

void instantiateInputMedia(const fs::path &path,
                           std::unique_ptr<InputMedia> &output);

} // namespace encoder
} // namespace icov
