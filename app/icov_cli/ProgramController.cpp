#include "ProgramController.h"
#include <iostream>

#include "app_common/program/icov_program_options.h"
#include "modes/BinaryMode.h"
#include "modes/CheckMode.h"
#include "modes/DecodeMode.h"
#include "modes/EncodeMode.h"
#include "modes/ExportMode.h"
#include "modes/GenerateGOPMode.h"
#include "modes/SymboleMode.h"

ProgramController::ProgramController()
    : IcovProgramBase("binary, symbole, export, check, encode-demo, "
                      "ordering-demo, encode, decode, extract-gop") {
  BinaryMode::AddProgramOptions(m_options_desc);
  SymboleMode::AddProgramOptions(m_options_desc);
  ExportMode::AddProgramOptions(m_options_desc);
  CheckMode::AddProgramOptions(m_options_desc);
}

void ProgramController::launchMode(const std::string &mode_id) {
  if (mode_id == "binary") {
    ICOV_DEBUG << "binary mode started";
    BinaryMode mode(m_options_map);
    mode.run();
    ICOV_DEBUG << "end binary\n";
  } else if (mode_id == "symbole") {
    ICOV_DEBUG << "symbole mode started";
    SymboleMode mode(m_options_map);
    mode.run();
    ICOV_DEBUG << "end symbole\n";
  } else if (mode_id == "export") {
    ICOV_DEBUG << "export mode started";
    ExportMode mode(m_options_map, codeLength(), dataPath());
    mode.run();
    ICOV_DEBUG << "end export\n";
  } else if (mode_id == "check") {
    ICOV_DEBUG << "check mode started";
    CheckMode mode(m_options_map, dataPath());
    mode.run();
    ICOV_DEBUG << "end check\n";
  } else if (mode_id == "encode" || mode_id == "icov") {
    ICOV_DEBUG << "encode mode started";
    EncodeMode mode(m_options_map);
    mode.run();
    ICOV_DEBUG << "end encode\n";
  } else if (mode_id == DecodeMode::name()) {
    ICOV_DEBUG << "decode mode started";
    DecodeMode mode(parseInputFile(args()), args());
    mode.run();
    ICOV_DEBUG << "end decode\n";
  } else if (mode_id == GenerateGOPMode::modeID()) {
    ICOV_DEBUG << "GenerateGOPMode mode started";
    GenerateGOPMode mode(args());
    mode.run();
    ICOV_DEBUG << "end GenerateGOPMode\n";
  } else {
    ICOV_THROW_INVALID_ARGUMENT("Unknown program mode: " + mode_id);
  }

  ICOV_DEBUG << "end program\n";
}
